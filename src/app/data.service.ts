import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  result:any;

  token:string = '?token=eb79967a-6a31-46c1-b6cc-bca6efbbdc61'
  mainURL:string = 'https://recruit-front-api.espeo.pl/inbox/messages';
  recipientsURL:string = 'https://recruit-front-api.espeo.pl/inbox/recipients';


  constructor(private _http: Http) { }

  getAllMessages() {
      return this._http.get(`${this.mainURL}${this.token}`)
      .map( result => this.result = result.json());
  }

  getMessage(messageId:string) {
      return this._http.get(`${this.mainURL}/${messageId}/${this.token}`)
      .map( result => this.result = result.json());
  }

  postMessage(message:any) {
      return this._http.post(`${this.mainURL}${this.token}`, message)
      .map( result => this.result = result.json());
  }

  patchMessage(message:any) {
      return this._http.patch(`${this.mainURL}${this.token}`, message)
      .map( result => this.result = result.json());
  }

  deleteMessage(message:any) {
      return this._http.patch(`${this.mainURL}${this.token}`, message)
      .map( result => this.result = result.json());
  }

  getRecipients() {
      return this._http.get(`${this.recipientsURL}${this.token}`)
      .map( result => this.result = result.json());
  }
}

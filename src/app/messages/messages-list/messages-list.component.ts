import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.scss']
})
export class MessagesListComponent implements OnInit {
  messages: any;
  color:string= 'transparent';

  constructor(private _data: DataService) { }

  deleteMessage(msg) {
    this._data.deleteMessage(msg)
      .subscribe(res => {
        console.log(res)
      })
  }

  readMessage(msg) {
    this._data.patchMessage(msg)
      .subscribe(res => {
        console.log(res)
      })
  }

  ngOnInit() {
    this._data.getAllMessages()
      .subscribe(res => {
        this.messages = res.filter((item) => {
          if (item.recipient !== 'Dawid Głośny'){
            return item;
          }
          if (item.displayed == null) {
            this.color = '#e0e0e0';
          }
        })
      });
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DataService } from '../../data.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {
  message: { id:number };
  messageDetails:any;

  constructor(private route: ActivatedRoute, private _data: DataService) { }

  ngOnInit() {
      this._data.getMessage(this.route.snapshot.params['id'])
      .subscribe(res => {
        this.messageDetails = res
      });

    this.message = {
      id: this.route.snapshot.params['id']
    };

    this.route.params
      .subscribe(
      (params: Params) => {
        this.message.id = params['id']
      }
      )
  }
}

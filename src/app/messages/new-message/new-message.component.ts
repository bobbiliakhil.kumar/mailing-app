import { Component, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.scss']
})
export class NewMessageComponent implements OnInit {
  messageForm: FormGroup;
  recipients:any;

  constructor(private _data: DataService) { }

  ngOnInit() {
    this.messageForm = new FormGroup({
      'sender': new FormControl('Dawid Głośny'),
      'recipient': new FormControl(null),
      'subject': new FormControl(null),
      'message': new FormControl(null)
    })

    this._data.getRecipients()
      .subscribe(res => {
        this.recipients = res
      })
  }

  sendMessage() {
    this._data.postMessage(this.messageForm.value)
    .subscribe(status => {
        console.log('status => ', status)
    });
  }
}
